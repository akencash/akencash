
Debian
====================
This directory contains files used to package akencashd/akencash-qt
for Debian-based Linux systems. If you compile akencashd/akencash-qt yourself, there are some useful files here.

## akencash: URI support ##


akencash-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install akencash-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your akencash-qt binary to `/usr/bin`
and the `../../share/pixmaps/akencash128.png` to `/usr/share/pixmaps`

akencash-qt.protocol (KDE)

