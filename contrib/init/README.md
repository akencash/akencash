Sample configuration files for:

SystemD: akencashd.service
Upstart: akencashd.conf
OpenRC:  akencashd.openrc
         akencashd.openrcconf
CentOS:  akencashd.init
OS X:    org.akencash.akencashd.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
